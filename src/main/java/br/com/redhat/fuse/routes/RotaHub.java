package br.com.redhat.fuse.routes;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

@Component
public class RotaHub extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		restConfiguration()
			.host("{{server.address}}")
//				.port("8081")
					.contextPath("api")
						.enableCORS(true)
							.component("servlet")
//							.component("restlet")
								.bindingMode(RestBindingMode.json)
									.dataFormatProperty("prettyPrint", "true");

		from("rest:get:simple:hello")
			.to("direct:simple-status");
	
		from("rest:get:simple/{id}")
			.log("${header.id}")
				.to("direct:simple-chained");
		
		from("timer://foo?fixedRate=true&period=60000").log("Processando ${body}").to("mock:foo");
	}

}
